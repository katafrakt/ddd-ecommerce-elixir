defmodule Kontor.SharedKernel.Values.WeightTest do
  use ExUnit.Case, async: true

  alias Kontor.SharedKernel.Values.Weight

  describe "convert/2" do
    test "grams to kilograms" do
      weight = Weight.build(700, :g)
      assert Weight.convert(weight, :kg) == Weight.build("0.7", :kg)
    end

    test "kilograms to grams" do
      weight = Weight.build(700, :kg)
      assert Weight.convert(weight, :g) == Weight.build(700_000, :g)
    end

    test "kilograms to kilograms" do
      weight = Weight.build(700, :kg)
      assert Weight.convert(weight, :kg) == Weight.build(700, :kg)
    end
  end
end
