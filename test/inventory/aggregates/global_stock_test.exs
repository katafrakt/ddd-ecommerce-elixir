defmodule Kontor.Inventory.Aggregates.GlobalStockTest do
  use ExUnit.Case, async: true
  alias Kontor.Inventory.Aggregates.GlobalStock
  alias Kontor.Inventory.Aggregates.Warehouse
  alias Kontor.SharedKernel.Entities.Product

  describe "get_product_quantity/2" do
    test "return 0 if product not in any warehouse" do
      assert GlobalStock.get_product_quantity(%GlobalStock{}, 100) == 0
    end

    test "sum quantities" do
      stock =
        setup_warehouses([
          [{%Product{id: 1}, 10}, {%Product{id: 2}, 15}],
          [{%Product{id: 1}, 2}],
          [{%Product{id: 1}, 0}]
        ])

      assert GlobalStock.get_product_quantity(stock, 1) == 12
    end

    test "sum when product not available in one warehouse quantities" do
      stock =
        setup_warehouses([
          [{%Product{id: 1}, 10}, {%Product{id: 2}, 15}],
          [{%Product{id: 2}, 2}],
          [{%Product{id: 1}, 0}]
        ])

      assert GlobalStock.get_product_quantity(stock, 1) == 10
    end

    test "treat negative value as zero" do
      stock =
        setup_warehouses([
          [{%Product{id: 1}, 10}, {%Product{id: 2}, 15}],
          [{%Product{id: 1}, 12}],
          [{%Product{id: 1}, -6}]
        ])

      assert GlobalStock.get_product_quantity(stock, 1) == 22
    end
  end

  describe "product_available?/2" do
    test "product does not exist in any warehouse" do
      refute GlobalStock.product_available?(%GlobalStock{}, 100)
    end

    test "product exists, but with 0 or negative stock" do
      stock =
        setup_warehouses([
          [{%Product{id: 1}, 0}, {%Product{id: 2}, 15}],
          [{%Product{id: 1}, 0}]
        ])

      refute GlobalStock.product_available?(stock, 1)
    end

    test "product exists with more negative stock than positive" do
      stock =
        setup_warehouses([
          [{%Product{id: 1}, 6}, {%Product{id: 2}, 15}],
          [{%Product{id: 1}, -8}]
        ])

      assert GlobalStock.product_available?(stock, 1)
    end

    test "product exists with only positive stock" do
      stock =
        setup_warehouses([
          [{%Product{id: 1}, 6}, {%Product{id: 2}, 15}],
          [{%Product{id: 1}, 8}]
        ])

      assert GlobalStock.product_available?(stock, 1)
    end
  end

  defp setup_warehouses(definitions) do
    warehouses =
      Enum.map(definitions, fn warehouse ->
        stock =
          Enum.map(warehouse, fn {product, quantity} ->
            %Warehouse.Stock{product: product, quantity: quantity}
          end)

        %Warehouse{stock: stock}
      end)

    %GlobalStock{warehouses: warehouses}
  end
end
