defmodule Kontor.Inventory.Aggregates.WarehouseTest do
  use ExUnit.Case, async: true

  alias Kontor.Inventory.Aggregates.Warehouse
  alias Kontor.SharedKernel.Entities.Product

  describe "get_product_quantity/2" do
    test "existing product" do
      warehouse = %Warehouse{
        stock: [
          %Warehouse.Stock{product: %Product{id: 1}, quantity: 5},
          %Warehouse.Stock{product: %Product{id: 2}, quantity: 15}
        ]
      }

      assert Warehouse.get_product_quantity(warehouse, 1) == 5
    end

    test "non-existing product" do
      warehouse = %Warehouse{}
      assert Warehouse.get_product_quantity(warehouse, 1) == {:error, :not_found}
    end
  end

  describe "add_products/3" do
    test "add to empty warehouse" do
      warehouse = Warehouse.add_products(%Warehouse{}, %Product{id: 1}, 15)

      assert warehouse.stock == [
               %Warehouse.Stock{
                 product: %Product{id: 1},
                 quantity: 15
               }
             ]
    end

    test "add to existing stock" do
      warehouse =
        setup_warehouse([
          {%Product{id: 1, name: "Beer"}, 16}
        ])
        |> Warehouse.add_products(%Product{id: 1}, 10)

      assert warehouse.stock == [
               %Warehouse.Stock{
                 product: %Product{id: 1},
                 quantity: 26
               }
             ]
    end

    test "replace product definition" do
      warehouse =
        setup_warehouse([
          {%Product{id: 1, name: "Beer"}, 16}
        ])
        |> Warehouse.add_products(%Product{id: 1, name: "Cola"}, 10)

      assert warehouse.stock == [
               %Warehouse.Stock{
                 product: %Product{id: 1, name: "Cola"},
                 quantity: 26
               }
             ]
    end
  end

  describe "remove_products/3" do
    test "remove stock from one product" do
      warehouse = setup_warehouse([{%Product{id: 1}, 10}, {%Product{id: 2}, 15}])
      warehouse = Warehouse.remove_products(warehouse, 1, 5)
      assert Warehouse.get_product_quantity(warehouse, 1) == 5
      assert Warehouse.get_product_quantity(warehouse, 2) == 15
    end

    test "non-existing product" do
      warehouse = setup_warehouse([])
      assert Warehouse.remove_products(warehouse, 1, 1) == {:error, :not_found}
    end

    test "too few products" do
      warehouse = setup_warehouse([{%Product{id: 1}, 10}])
      assert Warehouse.remove_products(warehouse, 1, 15) == {:error, :too_few}
    end
  end

  defp setup_warehouse(products) when is_list(products) do
    stock =
      Enum.map(products, fn {product, quantity} ->
        %Warehouse.Stock{product: product, quantity: quantity}
      end)

    %Warehouse{stock: stock}
  end
end
