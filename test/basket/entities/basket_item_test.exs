defmodule Kontor.Basket.Entities.BasketItemTest do
  use ExUnit.Case, async: true

  alias Kontor.Basket.Entities.BasketItem
  alias Kontor.Basket.Values.Price
  alias Kontor.SharedKernel.Entities.Product

  @item %BasketItem{
    product: %Product{id: 1},
    unit_price: Price.new("0.99", "1.50"),
    quantity: 15,
    tax: Decimal.new("0.23")
  }

  describe "total_price/1" do
    test "calculate total price" do
      assert BasketItem.total_price(@item) == Decimal.new("14.85")
    end
  end

  describe "total_price_before_discount/1" do
    test "calculate total price before discount" do
      assert BasketItem.total_price_before_discount(@item) ==
               Decimal.new("22.50")
    end
  end

  describe "total_price_after_taxes/1" do
    test "calculate total price after taxes" do
      assert BasketItem.total_price_after_taxes(@item) ==
               Decimal.new("18.27")
    end
  end

  describe "has_product_id?/2" do
    test "has product" do
      assert BasketItem.has_product_id?(@item, 1)
    end

    test "does not have item" do
      refute BasketItem.has_product_id?(@item, 2)
    end
  end
end
