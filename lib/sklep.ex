defmodule Kontor do
  @moduledoc """
  Documentation for `Kontor`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Kontor.hello()
      :world

  """
  def hello do
    :world
  end
end
