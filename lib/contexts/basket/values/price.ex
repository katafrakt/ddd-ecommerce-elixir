defmodule Kontor.Basket.Values.Price do
  @moduledoc false

  use TypedStruct
  alias __MODULE__

  @type value :: Decimal.t()

  @enforce_keys [:value, :value_before_discount]
  typedstruct do
    field :value, value()
    field :value_before_discount, value()
  end

  @spec new(value()) :: Price.t()
  @spec new(value(), value()) :: Price.t()
  def new(value) do
    %Price{value: value, value_before_discount: value}
  end

  def new(value, value_before_discount) do
    %Price{value: value, value_before_discount: value_before_discount}
  end
end
