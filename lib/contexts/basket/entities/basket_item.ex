defmodule Kontor.Basket.Entities.BasketItem do
  @moduledoc """
  Entity representing a single line item in a basket - a product, its price and quantity
  """

  use TypedStruct
  alias __MODULE__
  alias Kontor.Basket.Values.Price
  alias Kontor.SharedKernel.Entities.Product

  @type quantity :: non_neg_integer()
  @type price :: Decimal.t()
  @type tax :: Decimal.t()

  typedstruct do
    field :id, non_neg_integer()
    field :product, Product.t()
    field :quantity, quantity()
    field :unit_price, Price.t()
    field :tax, tax()
  end

  @doc "Returns total price for a basket item"
  @spec total_price(BasketItem.t()) :: price()
  def total_price(basket_item) do
    Decimal.mult(basket_item.unit_price.value, basket_item.quantity)
  end

  @doc "Returns total price before discount for an item"
  @spec total_price_before_discount(BasketItem.t()) :: price()
  def total_price_before_discount(basket_item) do
    Decimal.mult(basket_item.unit_price.value_before_discount, basket_item.quantity)
  end

  @doc "Returns total_price after taxes"
  @spec total_price_after_taxes(BasketItem.t()) :: price()
  def total_price_after_taxes(basket_item) do
    Decimal.add(1, basket_item.tax)
    |> Decimal.mult(BasketItem.total_price(basket_item))
    |> Decimal.round(2)
  end

  @doc "Returns whether its product has a given id"
  @spec has_product_id?(BasketItem.t(), integer()) :: boolean()
  def has_product_id?(item = %BasketItem{}, id) do
    item.product.id == id
  end
end
