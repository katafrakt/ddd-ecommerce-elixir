defmodule Kontor.SharedKernel.Entities.Product do
  @moduledoc """
  A physical product stored in a warehouse
  """

  alias Kontor.SharedKernel.Values.Weight

  use TypedStruct

  typedstruct do
    field :id, non_neg_integer()
    field :name, String.t()
    field :weight, Weight.t()
  end
end
