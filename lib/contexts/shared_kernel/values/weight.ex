defmodule Kontor.SharedKernel.Values.Weight do
  @moduledoc false

  alias __MODULE__
  use TypedStruct

  @type value :: Decimal.t()
  @type unit :: :kg | :g

  typedstruct do
    field :value, value()
    field :unit, unit()
  end

  @spec new(value(), unit()) :: Weight.t()
  def new(value, unit) do
    %Weight{value: value, unit: unit}
  end

  @spec convert(Weight.t(), unit()) :: Weight.t()
  def convert(weight, unit) do
    new_value =
      case {weight.unit, unit} do
        {value, value} -> weight.value
        {:kg, :g} -> Decimal.mult(weight.value, 1000)
        {:g, :kg} -> Decimal.div(weight.value, 1000)
      end

    Weight.new(new_value, unit)
  end

  @doc "A convenience function to build a weight quickly"
  @spec build(String.t() | integer(), unit()) :: Weight.t()
  def build(value, unit) do
    Weight.new(Decimal.new(value), unit)
  end
end
