defmodule Kontor.Inventory.Aggregates.GlobalStock do
  @moduledoc false

  use TypedStruct
  alias __MODULE__
  alias Kontor.Inventory.Aggregates.Warehouse

  @type quantity :: integer()
  @type product_id :: integer()

  typedstruct do
    field :warehouses, list(Warehouse.t()), default: []
  end

  @doc """
  Return a combined stock from all warehouses, but treats warehouses with negativ
  stock quantity as if the wuantity is equal to zero.
  """
  @spec get_product_quantity(GlobalStock.t(), product_id()) :: quantity()
  def get_product_quantity(stock = %GlobalStock{}, product_id) do
    stock.warehouses
    |> Enum.map(&Warehouse.get_product_quantity(&1, product_id))
    |> Enum.map(&translate_warehouse_stock/1)
    |> Enum.sum()
  end

  @doc "Return if product is available in any warehouse"
  @spec product_available?(GlobalStock.t(), product_id()) :: boolean()
  def product_available?(stock = %GlobalStock{}, product_id) do
    get_product_quantity(stock, product_id) > 0
  end

  defp translate_warehouse_stock({:error, :not_found}), do: 0
  defp translate_warehouse_stock(num) when num < 0, do: 0
  defp translate_warehouse_stock(num), do: num
end
