defmodule Kontor.Inventory.Aggregates.Warehouse do
  @moduledoc false

  use TypedStruct
  alias __MODULE__
  alias Kontor.SharedKernel.Entities.Product

  @type quantity :: integer()
  @type product_id :: non_neg_integer()

  typedstruct module: Stock do
    field :product, Product.t()
    field :quantity, Warehouse.quantity()
  end

  typedstruct do
    field :name, String.t()
    field :stock, list(Stock.t()), default: []
  end

  @doc "Return stock for given product in the warehouse"
  @spec get_product_quantity(Warehouse.t(), product_id()) :: quantity()
  def get_product_quantity(warehouse = %Warehouse{}, product_id) do
    case get_stock(warehouse, product_id) do
      {:ok, stock} -> stock.quantity
      error -> error
    end
  end

  @doc "Add a number of products to the stock"
  @spec add_products(Warehouse.t(), Product.t(), non_neg_integer()) :: Warehouse.t()
  def add_products(warehouse = %Warehouse{}, product, quantity) when quantity > 0 do
    new_quantity =
      case get_stock(warehouse, product.id) do
        {:error, :not_found} -> quantity
        {:ok, stock} -> stock.quantity + quantity
      end

    new_stock = [
      %Warehouse.Stock{product: product, quantity: new_quantity}
      | Enum.reject(warehouse.stock, &(&1.product.id == product.id))
    ]

    %Warehouse{warehouse | stock: new_stock}
  end

  @doc "Remove a number of products from the stock"
  @spec remove_products(Warehouse.t(), product_id(), non_neg_integer()) ::
          Warehouse.t() | {:error, :too_few} | {:error, :not_found}
  def remove_products(warehouse = %Warehouse{}, product_id, quantity) do
    with {:ok, stock} <- get_stock(warehouse, product_id) do
      if stock.quantity - quantity < 0 do
        {:error, :too_few}
      else
        new_stock = [
          %Warehouse.Stock{product: stock.product, quantity: stock.quantity - quantity}
          | Enum.reject(warehouse.stock, &(&1.product.id == product_id))
        ]

        %Warehouse{warehouse | stock: new_stock}
      end
    end
  end

  defp get_stock(warehouse, product_id) do
    case Enum.find(warehouse.stock, &(&1.product.id == product_id)) do
      nil -> {:error, :not_found}
      stock -> {:ok, stock}
    end
  end
end
